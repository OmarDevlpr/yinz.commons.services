import LogModel from "@yinz/commons.data/ts/LogModel";
import { Entity, Column } from "typeorm";

@Entity()
export default class LoggedServiceLog extends LogModel {

    @Column()
    avatarName: string;
}