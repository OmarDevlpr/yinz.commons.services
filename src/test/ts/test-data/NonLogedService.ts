import ServiceTemplate, { ServiceTemplateOptions } from "../../../main/ts/ServiceTemplate";
import { DefaultServiceReq, DefaultServiceRsp } from "../../../main/ts/ServiceTemplate";
import { YinzOptions, Exception } from "@yinz/commons";
import { YinzConnection, YinzTransaction } from "@yinz/commons.data/ts/DataSource";

export interface NonLoggedServiceReq extends DefaultServiceReq {
    avatarName: string;
};
export interface NonLoggedServiceRsp extends DefaultServiceRsp { };
export interface NonLoggedServiceOptions extends ServiceTemplateOptions { };


export default class NonLoggedService extends ServiceTemplate<NonLoggedServiceReq, NonLoggedServiceRsp> {

    constructor(options: NonLoggedServiceOptions) {

        options.serviceName = "NonLoggedService";

        super(options);
    }

    protected valReqRemFlds(req: NonLoggedServiceReq, options?: YinzOptions): void {

        if (req.avatarName.length < 10) {

            throw new Exception("ERR_COMNS_SERVCS__TEST_NON_LOG_SERVICE_INV_REQ__AVATAR_NAME", {
                message: `the provided avatarName ${req.avatarName} is invalid! it should be of length minimum 10.`,
                req: req,
                avatarName: req.avatarName
            });

        }

    }

    protected async executeService(handler: YinzConnection | YinzTransaction, req: NonLoggedServiceReq, options?: YinzOptions): Promise<NonLoggedServiceRsp> {
        return {};
    }

}