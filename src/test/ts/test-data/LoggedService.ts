import ServiceTemplate, { ServiceTemplateOptions } from "../../../main/ts/ServiceTemplate";
import { DefaultServiceReq, DefaultServiceRsp} from "../../../main/ts/ServiceTemplate";
import { YinzOptions, Exception } from "@yinz/commons";
import { YinzConnection, YinzTransaction } from "@yinz/commons.data/ts/DataSource";

export interface LoggedServiceReq extends DefaultServiceReq {
    avatarName: string;
 } ;
export interface LoggedServiceRsp extends DefaultServiceRsp  { } ;
export interface LoggedServiceOptions extends ServiceTemplateOptions { };


export default class LoggedService extends ServiceTemplate<LoggedServiceReq, LoggedServiceRsp> {

    constructor( options: LoggedServiceOptions) {

        options.serviceName = "LoggedService";
        options.logReqRsp   = true;

        super(options);
    }

    protected valReqRemFlds(req: LoggedServiceReq, options?: YinzOptions): void {
                
        if ( req.avatarName.length < 10 ) {

            throw new Exception("ERR_COMNS_SERVCS__TEST_LOG_SERVICE_INV_REQ__AVATAR_NAME", {
                message: `the provided avatarName ${req.avatarName} is invalid! it should be of length minimum 10.`,
                req: req,
                avatarName: req.avatarName
            });
            
        }        
        
    }

    protected async executeService(handler: YinzConnection | YinzTransaction, req: LoggedServiceReq, options?: YinzOptions): Promise<LoggedServiceRsp> {        
        return {};    
    }

}