// libs
import * as assert from "assert";
import { Chance } from "chance";
import Container from "@yinz/container/ts/Container";
import { TypeormDao, TypeormDataSource } from "@yinz/commons.data";
import { YinzConnection, YinzTransaction } from "@yinz/commons.data/ts/DataSource";
import LoggedService from "./test-data/LoggedService";
import { Logger } from "@yinz/commons";
import AccessManager from "@yinz/access.data/ts/AccessManager";
import LoggedServiceLog from "./test-data/LoggedServiceLog";
import NonLoggedService from "./test-data/NonLogedService";
import Profile from "@yinz/access.data/models/Profile";
import Authz from "@yinz/access.data/models/Authz";
import User from "@yinz/access.data/models/User";


const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        '@yinz/commons.data/ts',
        '@yinz/access.data/ts',
        process.cwd() + '/dist/main/ts'
    ],
});

const chance = new Chance();
// const Exception = container.getClazz('Exception');

let logger        = container.getBean<Logger>("logger");
let accessManager = container.getBean<AccessManager>("accessManager");
let dataSource    = container.getBean<TypeormDataSource>('typeormDataSource');

let profileDao    = container.getBean<TypeormDao<Profile>>("profileDao");
let authzDao      = container.getBean<TypeormDao<Authz>>("authzDao");
let userDao       = container.getBean<TypeormDao<User>>("userDao");


let loggedServiceLogDao = new TypeormDao<LoggedServiceLog>({
    model: LoggedServiceLog,
    logger,
    accessManager
})

let loggedService = new LoggedService({
    logger: logger,
    accessManager: accessManager,
    logDao: loggedServiceLogDao
});

let nonLoggedService = new NonLoggedService({
    logger: logger,
    accessManager: accessManager    
});

container.setBean("loggedService", loggedService);
container.setBean("nonLoggedService", nonLoggedService);
container.setBean("loggedServiceLogDao", loggedServiceLogDao);

container.setClazz("LoggedService", LoggedService);
container.setClazz("NonLoggedService", NonLoggedService);
container.setClazz("LoggedServiceLog", LoggedServiceLog)

let conn: YinzConnection;
let trans: YinzTransaction;



describe('| access.data.AuthzGroupTest', function () {

    before(() => {
        return new Promise(async (resolve) => {
            conn = await dataSource.getConn();
            resolve(conn);
        })
    });

    after(() => {
        return new Promise(async (resolve) => {
            await dataSource.relConn(conn)
            resolve(true);
        })
    });

    beforeEach(() => {
        return new Promise(async (resolve) => {
            trans = await dataSource.startTrans(conn);
            resolve(trans);
        })
    });

    afterEach(() => {
        return new Promise(async (resolve) => {
            await dataSource.rollbackTrans(trans)
            resolve(true);
        })
    });

    it('| # (Logged Service) --> should execute service successfully ', async () => {


      // 1. prepare test-data  
      let req = {
          avatarName: chance.string({length: 10}),
          reqUser: "__anonymous__"        
      }

      let options = { user: "__super_user__" };
      
      // 2. execute test
      let rsp = await loggedService.process(trans, req, options)

      
      // 3.1. check that response exists with default fields

      assert.ok(rsp)
      assert.ok(rsp.reqRef)
      assert.ok(rsp.rspCode)
      assert.ok(rsp.rspRef)
      assert.ok(rsp.rspSysDate)
      assert.ok(rsp.reqSysDate)      

      assert.strictEqual(rsp.rspCode, "A");

      // 3.2 check that request and resposne was logged was logged

      let logFindResult = await loggedServiceLogDao.findAll(trans, options)

      assert.ok(logFindResult)
      assert.ok(logFindResult[0])

      assert.strictEqual(logFindResult.length, 1)
      assert.strictEqual(logFindResult[0].avatarName, req.avatarName)
      assert.strictEqual(logFindResult[0].reqRef, rsp.reqRef)
      assert.strictEqual(logFindResult[0].rspCode, rsp.rspCode)
      assert.strictEqual(logFindResult[0].rspRef, rsp.rspRef)
      assert.strictEqual(logFindResult[0].rspSysDate + "", rsp.rspSysDate + "")
      assert.strictEqual(logFindResult[0].reqSysDate + "", rsp.reqSysDate + "")

    });


    it('| # (Logged Service) --> should give a response code [D] as there is a validation error ', async () => {


        // 1. prepare test-data  
        let req = {
            avatarName: chance.string({ length: 1 }),
            reqUser: "__anonymous__"
        }

        let options = { user: "__super_user__" };

        // 2. execute test
        let rsp = await loggedService.process(trans, req, options)


        // 3.1. check that response exists with default fields

        assert.ok(rsp)
        assert.ok(rsp.reqRef)
        assert.ok(rsp.rspCode)
        assert.ok(rsp.rspRef)
        assert.ok(rsp.rspSysDate)
        assert.ok(rsp.reqSysDate)

        assert.strictEqual(rsp.rspCode, "D");

        // 3.2 check that request and resposne was logged was logged

        let logFindResult = await loggedServiceLogDao.findAll(trans, options)

        assert.ok(logFindResult)
        assert.ok(logFindResult[0])

        assert.strictEqual(logFindResult.length, 1)
        assert.strictEqual(logFindResult[0].avatarName, req.avatarName)
        assert.strictEqual(logFindResult[0].reqRef, rsp.reqRef)
        assert.strictEqual(logFindResult[0].rspCode, rsp.rspCode)
        assert.strictEqual(logFindResult[0].rspRef, rsp.rspRef)
        assert.strictEqual(logFindResult[0].rspReason, rsp.rspReason)
        assert.strictEqual(logFindResult[0].rspMessage, rsp.rspMessage)
        assert.strictEqual(rsp.rspReason, "ERR_COMNS_SERVCS__TEST_LOG_SERVICE_INV_REQ__AVATAR_NAME");
        assert.strictEqual(logFindResult[0].rspReason, "ERR_COMNS_SERVCS__TEST_LOG_SERVICE_INV_REQ__AVATAR_NAME");
        assert.strictEqual(logFindResult[0].rspExtra, rsp.rspExtra)        
        assert.strictEqual(logFindResult[0].rspSysDate + "", rsp.rspSysDate + "")
        assert.strictEqual(logFindResult[0].reqSysDate + "", rsp.reqSysDate + "")

    });


    it('| # (NonLogged Service) --> should execute service successfully ', async () => {


        // 1. prepare test-data  
        let req = {
            avatarName: chance.string({ length: 10 }),
            reqUser: "__anonymous__"
        }

        let options = { user: "__super_user__" };

        // 2. execute test
        let rsp = await nonLoggedService.process(trans, req, options)


        // 3.1. check that response exists with default fields

        assert.ok(rsp)
        assert.ok(rsp.reqRef)
        assert.ok(rsp.rspCode)
        assert.ok(rsp.rspRef)
        assert.ok(rsp.rspSysDate)
        assert.ok(rsp.reqSysDate)

        assert.strictEqual(rsp.rspCode, "A");

        // 3.2 check that request and resposne was not logged

        let logFindResult = await loggedServiceLogDao.findAll(trans, options)

        assert.ok(logFindResult)        

        assert.strictEqual(logFindResult.length, 0)        

    });

    it('| # (NonLogged Service) --> should give a response code [D] as there is a validation error ', async () => {


        // 1. prepare test-data  
        let req = {
            avatarName: chance.string({ length: 1 }),
            reqUser: "__anonymous__"
        }

        let options = { user: "__super_user__" };

        // 2. execute test
        let rsp = await nonLoggedService.process(trans, req, options)


        // 3.1. check that response exists with default fields

        assert.ok(rsp)
        assert.ok(rsp.reqRef)
        assert.ok(rsp.rspCode)
        assert.ok(rsp.rspRef)
        assert.ok(rsp.rspSysDate)
        assert.ok(rsp.reqSysDate)

        assert.strictEqual(rsp.rspCode, "D");

        // 3.2 check that request and resposne was not logged

        let logFindResult = await loggedServiceLogDao.findAll(trans, options)

        assert.ok(logFindResult)        

        assert.strictEqual(logFindResult.length, 0)        
        assert.strictEqual(rsp.rspReason, "ERR_COMNS_SERVCS__TEST_NON_LOG_SERVICE_INV_REQ__AVATAR_NAME");
        

    });


    it('| # Auth (Logged Service) --> should execute service successfully with super_user', async () => {


        // 1. prepare test-data  
        let req = {
            avatarName: chance.string({ length: 10 }),
            reqUser: "__anonymous__"
        }

        let options = { user: "__super_user__" };

        // 2. execute test
        let rsp = await loggedService.process(trans, req, options)


        // 3.1. check that response exists with default fields

        assert.ok(rsp)
        assert.ok(rsp.reqRef)
        assert.ok(rsp.rspCode)
        assert.ok(rsp.rspRef)
        assert.ok(rsp.rspSysDate)
        assert.ok(rsp.reqSysDate)

        assert.strictEqual(rsp.rspCode, "A");

        // 3.2 check that request and resposne was logged was logged

        let logFindResult = await loggedServiceLogDao.findAll(trans, options)

        assert.ok(logFindResult)
        assert.ok(logFindResult[0])

        assert.strictEqual(logFindResult.length, 1)
        assert.strictEqual(logFindResult[0].avatarName, req.avatarName)
        assert.strictEqual(logFindResult[0].reqRef, rsp.reqRef)
        assert.strictEqual(logFindResult[0].rspCode, rsp.rspCode)
        assert.strictEqual(logFindResult[0].rspRef, rsp.rspRef)
        assert.strictEqual(logFindResult[0].rspSysDate + "", rsp.rspSysDate + "")
        assert.strictEqual(logFindResult[0].reqSysDate + "", rsp.reqSysDate + "")

    });


    it('| # Auth (Logged Service) --> should execute service successfully with a valid User', async () => {



        // 1. prepare test-data  

        let userCode = chance.string(({ length: 10 }));

        let req = {
            avatarName: chance.string({ length: 10 }),
            reqUser: userCode
        }

        let options = { user: "__super_user__" };

        let authz = new Authz();        
        authz.name         = "Logged Service"
        authz.code         = "LoggedService";
        authz.allowExecute = true;

        authz = await authzDao.create(trans, authz, options);

        let profile = new Profile();
        profile.code       = chance.string(({length: 10}));
        profile.name       = chance.string(({length: 10}));
        profile.authzs     = [authz];

        profile = await profileDao.create(trans, profile, {...options, include: ["authzs"]});

        let user = new User();
        user.code          = userCode;
        user.status        = "A";
        user.profiles      = [profile]

        user = await userDao.create(trans, user, {...options, include: ['profiles']});
        

        // 2. execute test
        let rsp = await loggedService.process(trans, req, {});


        // 3.1. check that response exists with default fields

        assert.ok(rsp)
        assert.ok(rsp.reqRef)
        assert.ok(rsp.rspCode)
        assert.ok(rsp.rspRef)
        assert.ok(rsp.rspSysDate)
        assert.ok(rsp.reqSysDate)

        assert.strictEqual(rsp.rspCode, "A");

        // 3.2 check that request and resposne was logged was logged

        let logFindResult = await loggedServiceLogDao.findAll(trans, options)

        assert.ok(logFindResult)
        assert.ok(logFindResult[0])

        assert.strictEqual(logFindResult.length, 1)
        assert.strictEqual(logFindResult[0].avatarName, req.avatarName)
        assert.strictEqual(logFindResult[0].reqRef, rsp.reqRef)
        assert.strictEqual(logFindResult[0].rspCode, rsp.rspCode)
        assert.strictEqual(logFindResult[0].rspRef, rsp.rspRef)
        assert.strictEqual(logFindResult[0].rspSysDate + "", rsp.rspSysDate + "")
        assert.strictEqual(logFindResult[0].reqSysDate + "", rsp.reqSysDate + "")

    });


    it('| # (Logged Service) --> should execute service successfully with valid reqDate', async () => {



        // 1. prepare test-data  

        let userCode = chance.string(({ length: 10 }));

        let reqDate: any = "2018-04-13T05:00:14.813Z";

        let req = {
            avatarName: chance.string({ length: 10 }),
            reqUser: userCode,
            reqDate: reqDate
        }

        let options = { user: "__super_user__" };

        let authz = new Authz();
        authz.name = "Logged Service"
        authz.code = "LoggedService";
        authz.allowExecute = true;

        authz = await authzDao.create(trans, authz, options);

        let profile = new Profile();
        profile.code = chance.string(({ length: 10 }));
        profile.name = chance.string(({ length: 10 }));
        profile.authzs = [authz];

        profile = await profileDao.create(trans, profile, { ...options, include: ["authzs"] });

        let user = new User();
        user.code = userCode;
        user.status = "A";
        user.profiles = [profile]

        user = await userDao.create(trans, user, { ...options, include: ['profiles'] });


        // 2. execute test
        let rsp = await loggedService.process(trans, req);


        // 3.1. check that response exists with default fields

        assert.ok(rsp)
        assert.ok(rsp.reqRef)
        assert.ok(rsp.rspCode)
        assert.ok(rsp.rspRef)
        assert.ok(rsp.rspSysDate)
        assert.ok(rsp.reqSysDate)

        assert.strictEqual(rsp.rspCode, "A");

        // 3.2 check that request and resposne was logged was logged

        let logFindResult = await loggedServiceLogDao.findAll(trans, options)

        assert.ok(logFindResult)
        assert.ok(logFindResult[0])

        assert.strictEqual(logFindResult.length, 1)
        assert.strictEqual(logFindResult[0].avatarName, req.avatarName)
        assert.strictEqual(logFindResult[0].reqRef, rsp.reqRef)
        assert.strictEqual(logFindResult[0].rspCode, rsp.rspCode)
        assert.strictEqual(logFindResult[0].rspRef, rsp.rspRef)
        assert.strictEqual(logFindResult[0].rspSysDate + "", rsp.rspSysDate + "")
        assert.strictEqual(logFindResult[0].reqSysDate + "", rsp.reqSysDate + "")

    });


    it('| # (Logged Service) --> should give a response code [D] as the reqDate is invalid', async () => {



        // 1. prepare test-data  

        let userCode = chance.string(({ length: 10 }));

        let reqDate: any = {};

        let req = {
            avatarName: chance.string({ length: 10 }),
            reqUser: userCode,
            reqDate: reqDate
        }

        let options = { user: "__super_user__" };

        let authz = new Authz();
        authz.name = "Logged Service"
        authz.code = "LoggedService";
        authz.allowExecute = true;

        authz = await authzDao.create(trans, authz, options);

        let profile = new Profile();
        profile.code = chance.string(({ length: 10 }));
        profile.name = chance.string(({ length: 10 }));
        profile.authzs = [authz];

        profile = await profileDao.create(trans, profile, { ...options, include: ["authzs"] });

        let user = new User();
        user.code = userCode;
        user.status = "A";
        user.profiles = [profile]

        user = await userDao.create(trans, user, { ...options, include: ['profiles'] });


        // 2. execute test
        let rsp = await loggedService.process(trans, req, {});


        // 3.1. check that response exists with default fields

        assert.ok(rsp)
        assert.ok(rsp.reqRef)
        assert.ok(rsp.rspCode)
        assert.ok(rsp.rspRef)
        assert.ok(rsp.rspSysDate)
        assert.ok(rsp.reqSysDate)

        assert.strictEqual(rsp.rspCode, "D");

        // 3.2 check that request and resposne was not logged as validation failed at an early stage

        let logFindResult = await loggedServiceLogDao.findAll(trans, options)

        assert.ok(logFindResult.length === 0)                
        assert.strictEqual(rsp.rspReason, "ERR_COMN_SRV__SRV_TEMPL__VAL_REQ__INV_REQ_DATE");        

    });

    it('| # Auth (Logged Service) --> should give a response code [D] as the user is invalid', async () => {



        // 1. prepare test-data  

        let userCode = chance.string(({ length: 10 }));

        let req = {
            avatarName: chance.string({ length: 10 }),
            reqUser: userCode + "D"
        }

        let options = { user: "__super_user__" };

        let authz = new Authz();
        authz.name = "Logged Service"
        authz.code = "LoggedService";
        authz.allowExecute = true;

        authz = await authzDao.create(trans, authz, options);

        let profile = new Profile();
        profile.code = chance.string(({ length: 10 }));
        profile.name = chance.string(({ length: 10 }));
        profile.authzs = [authz];

        profile = await profileDao.create(trans, profile, { ...options, include: ["authzs"] });

        let user = new User();
        user.code = userCode;
        user.status = "A";
        user.profiles = [profile]

        user = await userDao.create(trans, user, { ...options, include: ['profiles'] });


        // 2. execute test
        let rsp = await loggedService.process(trans, req, {});


        // 3.1. check that response exists with default fields

        assert.ok(rsp)
        assert.ok(rsp.reqRef)
        assert.ok(rsp.rspCode)
        assert.ok(rsp.rspRef)
        assert.ok(rsp.rspSysDate)
        assert.ok(rsp.reqSysDate)

        assert.strictEqual(rsp.rspCode, "D");

        // 3.2 check that request and resposne was logged was logged

        let logFindResult = await loggedServiceLogDao.findAll(trans, options)

        assert.ok(logFindResult)
        assert.ok(logFindResult[0])

        assert.strictEqual(logFindResult.length, 1)
        assert.strictEqual(logFindResult[0].avatarName, req.avatarName)
        assert.strictEqual(logFindResult[0].reqRef, rsp.reqRef)
        assert.strictEqual(logFindResult[0].rspCode, rsp.rspCode)
        assert.strictEqual(logFindResult[0].rspRef, rsp.rspRef)
        assert.strictEqual(logFindResult[0].rspReason, rsp.rspReason)
        assert.strictEqual(logFindResult[0].rspMessage, rsp.rspMessage)
        assert.strictEqual(rsp.rspReason, "ER_ACCESS_MNG__LOOKUP_USER_AUTHZES__USER_NOT_FOUND");
        assert.strictEqual(logFindResult[0].rspReason, "ER_ACCESS_MNG__LOOKUP_USER_AUTHZES__USER_NOT_FOUND");
        assert.strictEqual(logFindResult[0].rspExtra, rsp.rspExtra)
        assert.strictEqual(logFindResult[0].rspSysDate + "", rsp.rspSysDate + "")
        assert.strictEqual(logFindResult[0].reqSysDate + "", rsp.reqSysDate + "")

    });


    it('| # Auth (Logged Service) --> should give a response code [D] as the user is null', async () => {



        // 1. prepare test-data  

        let userCode = chance.string(({ length: 10 }));

        let invCode: any = null;

        let req = {
            avatarName: chance.string({ length: 10 }),
            reqUser: invCode
        }

        let options = { user: "__super_user__" };

        let authz = new Authz();
        authz.name = "Logged Service"
        authz.code = "LoggedService";
        authz.allowExecute = true;

        authz = await authzDao.create(trans, authz, options);

        let profile = new Profile();
        profile.code = chance.string(({ length: 10 }));
        profile.name = chance.string(({ length: 10 }));
        profile.authzs = [authz];

        profile = await profileDao.create(trans, profile, { ...options, include: ["authzs"] });

        let user = new User();
        user.code = userCode;
        user.status = "A";
        user.profiles = [profile]

        user = await userDao.create(trans, user, { ...options, include: ['profiles'] });


        // 2. execute test
        let rsp = await loggedService.process(trans, req, {});


        // 3.1. check that response exists with default fields

        assert.ok(rsp)
        assert.ok(rsp.reqRef)
        assert.ok(rsp.rspCode)
        assert.ok(rsp.rspRef)
        assert.ok(rsp.rspSysDate)
        assert.ok(rsp.reqSysDate)

        assert.strictEqual(rsp.rspCode, "D");

        // 3.2 check that request and resposne was logged was logged

        let logFindResult = await loggedServiceLogDao.findAll(trans, options)

        assert.ok(logFindResult)
        assert.ok(logFindResult[0])

        assert.strictEqual(logFindResult.length, 1)
        assert.strictEqual(logFindResult[0].avatarName, req.avatarName)
        assert.strictEqual(logFindResult[0].reqRef, rsp.reqRef)
        assert.strictEqual(logFindResult[0].rspCode, rsp.rspCode)
        assert.strictEqual(logFindResult[0].rspRef, rsp.rspRef)
        assert.strictEqual(logFindResult[0].rspReason, rsp.rspReason)
        assert.strictEqual(logFindResult[0].rspMessage, rsp.rspMessage)
        assert.strictEqual(rsp.rspReason, "ERR_COMN_SRV__SRV_TEMPL__IS_ACESS_GRNTD__ACTION_NOT_GRANTED");
        assert.strictEqual(logFindResult[0].rspReason, "ERR_COMN_SRV__SRV_TEMPL__IS_ACESS_GRNTD__ACTION_NOT_GRANTED");
        assert.strictEqual(logFindResult[0].rspExtra, rsp.rspExtra)
        assert.strictEqual(logFindResult[0].rspSysDate + "", rsp.rspSysDate + "")
        assert.strictEqual(logFindResult[0].reqSysDate + "", rsp.reqSysDate + "")

    });

    it('| # Auth (Logged Service) --> should give a response code [D] as the user does not have allowExecute on service', async () => {



        // 1. prepare test-data  

        let userCode = chance.string(({ length: 10 }));

        let req = {
            avatarName: chance.string({ length: 10 }),
            reqUser: userCode 
        }

        let options = { user: "__super_user__" };

        let authz = new Authz();
        authz.name = "Logged Service"
        authz.code = "LoggedService";
        authz.allowExecute = false;

        authz = await authzDao.create(trans, authz, options);

        let profile = new Profile();
        profile.code = chance.string(({ length: 10 }));
        profile.name = chance.string(({ length: 10 }));
        profile.authzs = [authz];

        profile = await profileDao.create(trans, profile, { ...options, include: ["authzs"] });

        let user = new User();
        user.code = userCode;
        user.status = "A";
        user.profiles = [profile]

        user = await userDao.create(trans, user, { ...options, include: ['profiles'] });


        // 2. execute test
        let rsp = await loggedService.process(trans, req, {});


        // 3.1. check that response exists with default fields

        assert.ok(rsp)
        assert.ok(rsp.reqRef)
        assert.ok(rsp.rspCode)
        assert.ok(rsp.rspRef)
        assert.ok(rsp.rspSysDate)
        assert.ok(rsp.reqSysDate)

        assert.strictEqual(rsp.rspCode, "D");

        // 3.2 check that request and resposne was logged was logged

        let logFindResult = await loggedServiceLogDao.findAll(trans, options)

        assert.ok(logFindResult)
        assert.ok(logFindResult[0])

        assert.strictEqual(logFindResult.length, 1)
        assert.strictEqual(logFindResult[0].avatarName, req.avatarName)
        assert.strictEqual(logFindResult[0].reqRef, rsp.reqRef)
        assert.strictEqual(logFindResult[0].rspCode, rsp.rspCode)
        assert.strictEqual(logFindResult[0].rspRef, rsp.rspRef)
        assert.strictEqual(logFindResult[0].rspReason, rsp.rspReason)
        assert.strictEqual(logFindResult[0].rspMessage, rsp.rspMessage)
        assert.strictEqual(rsp.rspReason, "ERR_COMMONS_DATA__TYP_ORM_DAO__ACTION_NOT_GRANTED");
        assert.strictEqual(logFindResult[0].rspReason, "ERR_COMMONS_DATA__TYP_ORM_DAO__ACTION_NOT_GRANTED");
        assert.strictEqual(logFindResult[0].rspExtra, rsp.rspExtra)
        assert.strictEqual(logFindResult[0].rspSysDate + "", rsp.rspSysDate + "")
        assert.strictEqual(logFindResult[0].reqSysDate + "", rsp.reqSysDate + "")

    });



    it('| # Auth (Logged Service) --> should execute successfully with __$$actionIsGranted$$__ ', async () => {



        // 1. prepare test-data  

        let userCode = chance.string(({ length: 10 }));

        let req = {
            avatarName: chance.string({ length: 10 }),
            reqUser: userCode
        }

        let options = { user: "__super_user__" };

        let authz = new Authz();
        authz.name = "Logged Service"
        authz.code = "LoggedService";
        authz.allowExecute = false;

        authz = await authzDao.create(trans, authz, options);

        let profile = new Profile();
        profile.code = chance.string(({ length: 10 }));
        profile.name = chance.string(({ length: 10 }));
        profile.authzs = [authz];

        profile = await profileDao.create(trans, profile, { ...options, include: ["authzs"] });

        let user = new User();
        user.code = userCode;
        user.status = "A";
        user.profiles = [profile]

        user = await userDao.create(trans, user, { ...options, include: ['profiles'] });


        // 2. execute test
        let rsp = await loggedService.process(trans, req, { __$$actionIsGranted$$__: true});


        // 3.1. check that response exists with default fields

        assert.ok(rsp)
        assert.ok(rsp.reqRef)
        assert.ok(rsp.rspCode)
        assert.ok(rsp.rspRef)
        assert.ok(rsp.rspSysDate)
        assert.ok(rsp.reqSysDate)

        assert.strictEqual(rsp.rspCode, "A");

        // 3.2 check that request and resposne was logged was logged

        let logFindResult = await loggedServiceLogDao.findAll(trans, options)

        assert.ok(logFindResult)
        assert.ok(logFindResult[0])

        assert.strictEqual(logFindResult.length, 1)
        assert.strictEqual(logFindResult[0].avatarName, req.avatarName)
        assert.strictEqual(logFindResult[0].reqRef, rsp.reqRef)
        assert.strictEqual(logFindResult[0].rspCode, rsp.rspCode)
        assert.strictEqual(logFindResult[0].rspRef, rsp.rspRef)
        assert.strictEqual(logFindResult[0].rspSysDate + "", rsp.rspSysDate + "")
        assert.strictEqual(logFindResult[0].reqSysDate + "", rsp.reqSysDate + "")
    });
});
