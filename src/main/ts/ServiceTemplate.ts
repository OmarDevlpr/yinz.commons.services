import AccessManager from "@yinz/access.data/ts/AccessManager";
import {TypeormDao } from "@yinz/commons.data";
import LogModel from "@yinz/commons.data/ts/LogModel";
import { YinzConnection, YinzTransaction } from "@yinz/commons.data/ts/DataSource";
import {Exception, Asserts, YinzOptions, Logger } from "@yinz/commons";
import * as _ from "lodash";
import * as moment from "moment";


export interface ServiceTemplateOptions { 
    serviceName?: string;
    accessManager: AccessManager;
    logDao?: TypeormDao<LogModel>;
    logReqRsp?: boolean;
    logger: Logger;
}

export interface DefaultServiceReq {
    reqSysDate?: Date;
    reqRef?: string;
    reqDate?: Date;
    reqUser?: string;    
}

export interface DefaultServiceRsp {
    reqRef?: string;
    rspCode?: "A" | "D" | "E";
    rspReason?: string;
    rspMessage?: string;
    rspExtra?: string;
    reqSysDate?: Date;
    rspSysDate?: Date;
    rspRef?: string;
}


export default abstract class ServiceTemplate<Req extends DefaultServiceReq, Rsp extends DefaultServiceRsp> { 
   
    protected  _serviceName: string;
    protected  _accessManager: AccessManager;
    protected  _logDao: TypeormDao<LogModel>;
    protected  _logReqRsp: boolean;
    protected  _logger: Logger;

    constructor ( options: ServiceTemplateOptions ) {
        this._serviceName = options.serviceName || "ServiceTemplate";
        this._accessManager = options.accessManager;
        this._logReqRsp = options.logReqRsp || false;
        this._logger = options.logger;

        if ( options.logDao ) {            
            this._logDao = options.logDao;
        }
    }

    private fillRequestDflts(req: Req, options?: YinzOptions): void {
                
        req.reqSysDate = new Date();
        
        if ( !req.reqRef ) { 
            req.reqRef = _.random(1000000000000000, 1999999999999999).toString();
        }

        if ( req.reqDate && _.isString(req.reqDate) ) {
            const tmp = moment(req.reqDate, moment.ISO_8601);
            req.reqDate = tmp.isValid() ? tmp.toDate() : req.reqDate;
        }
        
    }

    private validateReqMinFlds(req: Req, options?: YinzOptions): void {

        Asserts.isTruthy(req, 'ERR_COMN_SRV__SRV_TEMPL__VAL_REQ__INV_REQ', {
            message: 'The supplied req ' + req + ' is invalid!',
            req: req,
        });

        if ( req.reqRef ) {
            Asserts.isNonEmptyString(req.reqRef, 'ERR_COMN_SRV__SRV_TEMPL__VAL_REQ__INV_REQ_REF', {
                message: 'The supplied reqRef ' + req.reqRef + ' of the supplied req is invalid!',
                reqRef: req.reqRef,
                req: req,
            });            
        }

        if ( req.reqDate ) {
            Asserts.isDate(req.reqDate, 'ERR_COMN_SRV__SRV_TEMPL__VAL_REQ__INV_REQ_DATE', {
                message: 'The supplied reqDate ' + req.reqDate + ' of the supplied req is invalid!',
                reqDate: req.reqDate,
                req: req,
            });
        }

        if ( req.reqUser ) {  
            Asserts.isNonEmptyString(req.reqUser, 'ERR_COMN_SRV__SRV_TEMPL__VAL_REQ__INV_REQ_USER', {
                message: 'The supplied reqUser ' + req.reqUser + ' of the supplied req is invalid! It should be a valid string.',
                reqUser: req.reqUser,
                req: req,
            });            
        }
    }

    protected async logReq(handler: YinzConnection | YinzTransaction, req: Req, options?: YinzOptions): Promise<LogModel> {

        let reqClone = Object.assign({}, req );
        
        return await this._logDao.create(handler, reqClone, options);

    }

    protected async logRsp(handler: YinzConnection | YinzTransaction, req: Req, log: LogModel, rsp: Rsp, options?: YinzOptions): Promise<void> {
        await this._logDao.updateById(handler, rsp, log.id, options);
    }
    protected valReqRemFlds(req: Req, options?: YinzOptions): void {
        return;
    }

    protected async assertBusinessRules(handler: YinzConnection | YinzTransaction, req: Req, options?: YinzOptions): Promise<void> {
        return;
    }

    protected async executeService(handler: YinzConnection | YinzTransaction, req: Req, options?: YinzOptions): Promise<Rsp> {
        return {} as Rsp;
    }

    protected async translateNegativeRsp(handler: YinzConnection | YinzTransaction, req: Req, rsp: Rsp, options?: YinzOptions): Promise<Rsp> {
        return rsp;
    }

    private fillRspDflts(req: Req, rsp: Rsp, options?: YinzOptions): void {

        rsp.rspSysDate = new Date() ;
        rsp.reqSysDate = req.reqSysDate ;
        rsp.rspRef     = rsp.rspRef     || req.reqRef;
        
    }

    private async isAccessGranted(handler: YinzConnection | YinzTransaction, serviceName: string, req: Req, options?: YinzOptions): Promise<void> {

        let user = req.reqUser || options && options.user;

        this._logger.trace('Enter <%s>.isAccessGranted(handler: %s, serviceName: %s, req: %j, options: %j)', handler, serviceName, req, options);

        if (!user ) {
            throw new Exception('ERR_COMN_SRV__SRV_TEMPL__IS_ACESS_GRNTD__ACTION_NOT_GRANTED', {
                message: `The user is missing! You should supply who is executing the request in options.`,
                user: req.reqUser,
                req: req,
                options: options
            });
        }

        if ( options && options.user === '__super_user__') {
            return;
        }

        if (options && options.__$$actionIsGranted$$__) {
            return;
        }

        let result = await this._accessManager.isUserGrantedAuthzes(handler, user, {
            subject: serviceName,
            action: "execute"
        });

        if (result === null) {
            throw new Exception('ERR_COMMONS_DATA__TYP_ORM_DAO__ACTION_NOT_GRANTED', {
                message: `the user [${req.reqUser}] is not granted [execute] on [${serviceName}]`,
                req: req,
                user: req.reqUser,
                options: options
            });
        }

        options = options || {};
        options.__$$actionIsGranted$$__ = true;

        this._logger.trace('Leave <%s>.isAccessGranted()', serviceName);

    }

    public async process(handler: YinzConnection | YinzTransaction, req: Req, options?: YinzOptions & any): Promise<Rsp> {
                
        let log: LogModel = new LogModel();
        let rsp: Rsp = {} as Rsp;        

        try {
            // 1. assign default values to the request
            this._logger.info("1. assign default values to the request...")
            this.fillRequestDflts(req, options);


            // 2. validate minimum requirements on the request
            this._logger.info("2. make sure that the request is good enough to work with...")
            this.validateReqMinFlds(req, options);


            // 3. log request if requested
            if ( !this._logReqRsp ) {
                this._logger.debug('3.1. The request and the response will not be logged as requested.');                            
            }
            else {            
                this._logger.info('3.2. log the request...');            
                log = await this.logReq(handler, req, {...options, user: "__super_user__"} );
            }


            // 4. verify that the user is allowed to execute the service
            this._logger.info(`4. make sure that [${req.reqUser} user is allowed to execute ${this._serviceName}`);            
            await this.isAccessGranted(handler, this._serviceName, req, options);


            // 5. validate remanining request requirements
            this._logger.info('5. make sure that the request is really good to work with...');
            this._logger.info('5.1. integrity checks...');
            this.valReqRemFlds(req, options);

            this._logger.info('5.2. business rules checks...');
            await this.assertBusinessRules(handler, req, options);

            // 6. execute the service 
            this._logger.info('6. execute the service...');
            let srvRsp: Rsp = await this.executeService(handler, req, options);

            rsp = srvRsp || {};
            rsp.reqRef = req.reqRef;
            rsp.rspCode = "A";

            // 7. return the response 
            this._logger.info('7. return the response...');

            return  rsp;


        } catch ( e ) {

            this._logger.warn(e.stack || e.message);
            
            rsp.reqRef      = req.reqRef;
            rsp.rspCode     = e instanceof Exception ? "D" : "E";
            rsp.rspReason   = e.reason;
            rsp.rspMessage  = e.message;
            rsp.rspExtra    = JSON.stringify(Object.assign({}, e.extra, {stack: e.stack}));            
            
            // 7. return the response 
            this._logger.info('7. transtalte then return negative response...');
            await this.translateNegativeRsp(handler, req, rsp, options);            
            return rsp;


        } finally {

            this.fillRspDflts(req, rsp, options);

            // 8. log response if request was logged
            this._logger.info('8. log the response, if requested...')
            
            if ( this._logReqRsp && log.id ) {
                await this.logRsp(handler, req, log, rsp, { ...options, user: "__super_user__" });
            }

        }
    }
    
}